# TremHunt

Starting to collect some material for a possible slimmed down or round based version of Tremulous/Unvanquished's gameplay.

TremHunt is the working title.

I gave a rough outline of what the Game / Mode could look like here: https://forums.unvanquished.net/viewtopic.php?t=2399

Right now, I've set up an (example) "Layout" that features:

* Two "mini-bases" for the Aliens side (2 eggs and 2 Acid tubes in each)
* An unreachable Overmind for the Aliens
* A minimal "Launch-point" base for the Humans (2 Telenodes, a Reactor, an Armoury and a lone Turret)

--> https://codeberg.org/SeanCJ/TremHunt/src/branch/main/MiniUnv-Arachnid2-v1 .dat (missing the extention atm... :/ )

That would be the approximate setup for the initial rounds (ie Rifles/SMG and Dretches only).

--------------------

Todo:

* Make more alternative Layouts (so there's some uncertainty and variety for the map)
* Work out how to set Build Points (ideally per side!), and disable Leeches and Drills :P.

    --> done :P. ``/g_BPInitialBudget 50``
    or, even better ``/g_BPInitialBudgetHumans 50``  (Aliens were ok with their 80, cause 2 bases)
    <br>Not worked out how to disable Drills + Leeches yet though!

-------------------------------

Next-Steps (rough):

* Get a working config or maprotation working that sets the Buildpoints (and ideally, disables Leeches+Drills too... :P )
* Set up a basic PvE (human) Maprotation "Campaign" - to familiarise myself with that system (and have something come out of it :P)
* Set up a mini-game (eg 3-6 rounds) for Arachnid - using identical Layouts to start with. (would require some kind of "no-respawn" mechanism to really work though...)
* **Alternate**: Set up a rotation (single map) with a variety of Layouts (if I'm feeling like making some Layouts :P)

Ok, should be looking pretty good then if I get that far, and I reckon I'll have some ideas by then on how I'd want to continue :P . Oh right --> maybe look at whether I can set up something similar for Tremulous also (without tooooo much trouble).